const jwt = require('jsonwebtoken');
const userModel = require('../users/user.model');
const express = require('express');
const router = express.Router();

router.post('/login',(req,res)=>{
const userName = req.body.username;
const password = req.body.password;
  if(!userName && !password){
    res.send({"Error":"Username and Password are required for login"});
  } else if (!userName){
    res.send({"Error":"Username required for login"});
  } else if (!password){
    res.send({"Error":"Password required for login"});
  } else {
    userModel.findOne({'username': userName,'password': password},'role username',(error,response)=>{
        if(!error && !response){
          res.send({"Error" : "No records found with this username and password.Try registering again"});
        }else if(error){
            res.send({"Error" : error});
            return
        }else if(response){
        const token=jwt.sign({role: response.role,username:response.username}, 'test-secret',{expiresIn: '2 days' })
        res.send({
            status: "Success",
            message: `A ${response.role} token generated successfully. Use this token with the request passing it as parameter with name Authorization`,
            token: token
        })
        }
    })
  } 
})
module.exports = router;