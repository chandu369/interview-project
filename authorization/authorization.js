const jwt = require('jsonwebtoken');

const authorize= {
    userAuthorize: (req,res,next) => {
        if(!req.query.Authorization){
            res.status(400).send({"Status":"Failed","Message":"Access denied. A valid token  to  be passed as parameter with name Authorization"})
        } else {
            let token = req.query.Authorization; 
            jwt.verify(token, 'test-secret', (err, decoded)=>{
            if(err){
                res.send({"Status":"Failed","Error":err});
            } else {
                req.decoded = decoded;
                next();
            }
            })
        }
    },
    adminAuthorize: (req,res,next) => {
        if(!req.query.Authorization){
            res.status(400).send({"Status":"Failed","Message":"Access denied. A valid Admin token  to  be passed as parameter with name Authorization"})
        } else {
            let token = req.query.Authorization; 
            jwt.verify(token, 'test-secret', {ignoreExpiration: false} , (err, decoded)=>{
            if(err) {
                res.send({"Status":"Failed","Error": err});
            } else if(decoded.role !== 'admin') {
                res.send({"Status":"Failed","Error":"A valid admin access token is required to access this resource"});
            } else {
                req.decoded = decoded;
                next();
            }
            })
        }
    }
}
module.exports = authorize;