const mongoose = require('mongoose');
const url = process.env.mongourl;

class MongoConfig {
    constructor(){
        this.mongoose  = mongoose
    }
    connect(){
        this.mongoose.connect(url,{dbName: process.env.database , useNewUrlParser: true});
          this.mongoose.connection.on('error',(err)=>{
                throw err;
          });
          this.mongoose.connection.once('open',() => {
              console.log("Connection established successfully for MongoDB");
          })
    }
    
}
module.exports = MongoConfig;