const dotenv = require('dotenv');
dotenv.config();
const mongoObject = require('../conn/conn.mongo'); 
const express = require('express');
const bodyParser = require('body-parser');
const userController = require('../users/user.controller');
const balancedController = require('../users/balanced.controller');
const adminController = require('../admin/admin.controller');
const loginController = require('../authorization/authentication');
const auth = require('../authorization/authorization');

class appConfig{
    constructor(){
        this.express = express;
        this.mongoConfig = new mongoObject();
    }

    setupexpress(){
        const app = express();
        app.use('/', this.checkIfAuthRequired, balancedController);//this is placed on top to void body parser from parsing as json
        app.use(bodyParser.json()); // for parsing application/json
        app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
        app.use('/', this.checkIfAuthRequired ,loginController);
        app.use('/', this.checkIfAuthRequired ,userController);
        app.use('/', this.checkIfAuthRequired ,adminController);
        app.listen(process.env.port);
        console.log(`Express server started on port ${process.env.port}`);
    }

    checkIfAuthRequired(req, res, next){
        if(req._parsedUrl.pathname.match(/\/user\/register/) || req._parsedUrl.pathname.match(/\/login/)){
            next();
        } else if(req._parsedUrl.pathname.match(/\/balanced/)){
            auth.userAuthorize(req, res, next);
        } else if(req._parsedUrl.pathname.match(/\/admin/)){
            auth.adminAuthorize(req, res, next);
        }
    }

    connectMongo(){
        this.mongoConfig.connect();
    }

    run(){
        this.setupexpress();
        this.connectMongo();
    }

}
module.exports = appConfig;

// "username":"test",
// "password":"1234",
// "role": "admin"