const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;
const user = new Schema({
    username: { type:String, unique: true, required: true },
    email: String,
    password: { type:String, unique: true, required: true },
    dob: Date,
    role: { type: String, default: 'user'}
}) 
const userModel = mongoose.model('user',user);
module.exports = userModel;