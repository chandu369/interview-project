const userModel = require('./user.model');
const balancedModel = require('./balanced.model');

class UserService{
    constructor(){
        this.balancedModel = balancedModel;
    }
    
    balanced(req, data, callback){
        if(this.checkBalanced(data)){
            this.balancedModel.findOneAndUpdate({username: req.decoded.username},{$inc :{attempts:1}},(err,result)=>{
                if(result){
                    callback({"username": result.username,"status":"Success","message":"Balanced","attempts":result.attempts +1});
                } else {
                    this.createBalancedEntry(req.decoded.username,(response)=>{
                        callback({"username": req.decoded.username,"status":"Success","message":"Balanced","attempts":1});
                    });
                }
            });
            
        } else {
            this.balancedModel.findOneAndUpdate({username: req.decoded.username},{$inc :{attempts:1}},(err,result)=>{
                if(result){
                    callback({"username": result.username,"status":"Success","message":"Unbalanced","attempts":result.attempts +1});
                } else {
                    this.createBalancedEntry(req.decoded.username, (response)=>{
                        callback({"username": req.decoded.username,"status":"Success","message":"Unbalanced","attempts":1});
                    });
                }
            });
        }
    }

    createBalancedEntry(username,callback){
        let newentry = this.balancedModel();
        newentry.username = username;
        newentry.save((err,saved)=>{
            callback();
        });
    }

    checkBalanced(data){
        if(data.length % 2 !==0){
            return false;
        } else {       
        for(let i=0;i < data.length/2;i++){
            if((data[i]==="[" && data[data.length -(i+1)]==="]") || (data[i]==="("&&data[data.length -(i+1)]===")") 
            || (data[i]==="{"&&data[data.length -(i+1)]==="}")){
                if(i === data.length/2-1){
                    return true;
                }
            } else {
                return false;
            }
        }}
    }
    register(req, callback){
        let body = req.body;
        if(!body.username && !body.password){
            callback({"Status":"Failed","Error":"Username and Password are mandatory for registration"});
        } else if(!body.username){
            callback({"Status":"Failed","Error":"Username mandatory for registration"});
        } else if(!body.password){
            callback({"Status":"Failed","Error":"Password mandatory for registration"});
        } else {
            this.userModel = new userModel();
            this.userModel.username = body.username;
            this.userModel.password = body.password;
            this.userModel.dob = body.dob;
            this.userModel.email = body.email;
            body.role==='admin' ? this.userModel.role = body.role : null;
            this.userModel.save((err, saved)=>{
                if(err){
                    err.code===11000 ?  callback({"Status":"Failed","Error":"A user already registered with the same username"}):callback({"Status":"Failed","Error":err});
                } else {
                    callback({"Status":"Success","Message": (body.role==="admin"? "Admin":"User") + " registered Successfully"});
                }
            })
        }
    }
}
module.exports = UserService;