const express = require('express');
const router = express.Router();
const serviceModule = require('./user.service');
const userService = new serviceModule();

router.post('/balanced',(req, res) => {
    var bodyStr = '';
    req.on("data",function(chunk){
        bodyStr += chunk.toString();
    });
    req.on("end",function(){
     userService.balanced(req, bodyStr, (callback)=>{
        res.send(callback);
     });
    });
})

module.exports = router;