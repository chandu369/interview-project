const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;
const balance = new Schema({
    username: { type:String, unique: true, required: true },
    attempts: { type: Number, default: 1}
}) 
const balancedModel = mongoose.model('balances',balance);
module.exports = balancedModel;