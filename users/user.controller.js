const express = require('express');
const router = express.Router();
const serviceModule = require('./user.service');
const userService = new serviceModule();

router.post('/user/register',(req, res) => {
    userService.register(req, (callback)=>{
        res.send(callback);
    });
})

module.exports = router;