const express = require('express');
const router = express.Router();
const serviceModule = require('./admin.service');
const adminService = new serviceModule();

router.get('/admin/users',(req, res) => {
     adminService.listUsers((callback)=>{
        res.send(callback);
     });
});

router.delete('/admin/user',(req, res) => {
    adminService.deleteUser(req, (callback)=>{
        res.send(callback);
     });
});

module.exports = router;