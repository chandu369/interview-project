const userModel = require('../users/user.model');

class AdminService{
    constructor(){
        this.userModel = userModel;
    }

    listUsers(callback){
        this.userModel.find({},{_id:0,__v:0,},(err,result)=>{
            callback(result);
        });
    }

    deleteUser(req,callback){
        let username = req.query.username;
        if(req.decoded.username===username){
            callback({"Status":"Failed","Message":"An admin cannot delete himself"});
            return
        }
        if(!username || username === null ){
            callback({"Status":"Failed","Message":`Username missing!!! Should be passed as queryparameter`});
        } else {
            this.userModel.deleteOne({username:username},(err, res)=>{
                if(res){
                    if(res.deletedCount > 0){
                        callback({"Status":"Success","Message":`User with username ${username} deleted successfully`});
                    } else if(res){
                        callback({"Status":"Failed","Message":`User with username ${username} doesn't exist`});
                    }
                }
            });
        }
    }
}
module.exports = AdminService